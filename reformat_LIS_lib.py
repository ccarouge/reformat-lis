import numpy as np
import netCDF4


class LIS6_Dataset(netCDF4.Dataset):
#    def __init__(self,filename):
#        self = netCDF4.Dataset(filename,"r+")

    @staticmethod
    def long_name_dict():
        ln_dict = { 'Swnet'     : 'net downward shortwave radiation'     \
                   ,'Lwnet'     : 'net downward longwave radiation'      \
                   ,'Qle'       : 'latent heat flux'                     \
                   ,'Qh'        : 'sensible heat flux'                   \
                   ,'Qg'        : 'soil heat flux'                       \
                   ,'Snowf'     : 'snowfall rate'                        \
                   ,'Rainf'     : 'total precipitation'                  \
                   ,'Evap'      : 'total evapotranspiration'             \
                   ,'Qs'        : 'surface runoff'                       \
                   ,'Qsb'       : 'subsurface runoff amount'             \
                   ,'Albedo'    : 'surface albedo'                       \
                   ,'SWE'       : 'snow water equivalent'                \
                   ,'SoilMoist' : 'soil moisture content'                \
                   ,'SoilTemp'  : 'soil temperature'                     \
                   ,'ECanop'    : 'interception evaporation'             \
                   ,'TVeg'      : 'vegetation transpiration'             \
                   ,'ESoil'     : 'bare soil evaporation'                \
                   ,'CanopInt'  : 'total canopy water storage'           \
                   ,'Wind_f'    : 'wind speed'                           \
                   ,'Rainf_f'   : 'rainfall flux'                        \
                   ,'Tair_f'    : 'air temperature'                      \
                   ,'Qair_f'    : 'specific humidity'                    \
                   ,'Psurf_f'   : 'surface pressure'                     \
                   ,'SWdown_f'  : 'surface downward shortwave radiation' \
                   ,'LWdown_f'  : 'surface downward longwave radiation'  \
                   ,'Landmask'  : 'landmask'                             \
                   ,'Landcover' : 'landcover'                            \
                   ,'SandFrac'  : 'fraction of sand'                     \
                   ,'ClayFrac'  : 'fraction of clay'                     \
                   ,'SiltFrac'  : 'fraction of silt'                     \
                   ,'Elevation' : 'elevation'                            \
                   ,'LAI'       : 'leaf area index'                      \
                   ,'lat'       : 'latitude'                             \
                   ,'lon'       : 'longitude'                            \
                  }
        return ln_dict

    @staticmethod
    def standard_name_dict():
        sn_dict = { 'Swnet'     : 'surface_net_downward_shortwave_flux'       \
                   ,'Lwnet'     : 'surface_net_downward_longwave_flux'        \
                   ,'Qle'       : 'surface_upward_latent_heat_flux'           \
                   ,'Qh'        : 'surface_upward_sensible_heat_flux'         \
                   ,'Qg'        : 'downward_heat_flux_in_soil'                \
                   ,'Snowf'     : 'snowfall_rate'                             \
                   ,'Rainf'     : 'precipitation_rate'                        \
                   ,'Evap'      : 'total_evapotranspiration'                  \
                   ,'Qs'        : 'surface_runoff_amount'                     \
                   ,'Qsb'       : 'subsurface_runoff_amount'                  \
                   ,'Albedo'    : 'surface_albedo'                            \
                   ,'SWE'       : 'liquid_water_content_of_surface_snow'      \
                   ,'SoilMoist' : 'soil_moisture_content'                     \
                   ,'SoilTemp'  : 'soil_temperature'                          \
                   ,'ECanop'    : 'interception_evaporation'                  \
                   ,'TVeg'      : 'vegetation_transpiration'                  \
                   ,'ESoil'     : 'bare_soil_evaporation'                     \
                   ,'CanopInt'  : 'total_canopy_water_storage'                \
                   ,'Wind_f'    : 'wind_speed'                                \
                   ,'Rainf_f'   : 'rainfall_flux'                             \
                   ,'Tair_f'    : 'air_temperature'                           \
                   ,'Qair_f'    : 'specific_humidity'                         \
                   ,'Psurf_f'   : 'surface_air_pressure'                      \
                   ,'SWdown_f'  : 'surface_downwelling_shortwave_flux_in_air' \
                   ,'LWdown_f'  : 'surface_downwelling_longwave_flux_in_air'  \
                   ,'Landmask'  : 'landmask'                                  \
                   ,'Landcover' : 'landcover'                                 \
                   ,'SandFrac'  : 'sandfrac'                                  \
                   ,'ClayFrac'  : 'clayfrac'                                  \
                   ,'SiltFrac'  : 'siltfrac'                                  \
                   ,'Elevation' : 'elevation'                                 \
                   ,'LAI'       : 'leaf_area_index'                           \
                   ,'lat'       : 'latitude'                                  \
                   ,'lon'       : 'longitude'                                 \
                  }
        return sn_dict

    def expand_time_avg(self, config):
        ''' Add _tavg for time averaged variable short names'''
        # Get only the short_name field
        array = config['short_name']

        # Get indices where we have time average variables
        indices = config.time_avg

        # Append short_name with _tavg for these variables
        for ni in np.nditer(indices):
            # Find variable that correspond to array[ni] (might have different cap / un-cap mix)!
            netcdf_name = config.match_config_netcdf_names(array[ni])
            self.renameVariable(netcdf_name, array[ni] + '_tavg')


    def expand_instant(self, config):
        ''' Add _inst for instantaneous variable short names'''
        # Get only the short_name field
        array = config['short_name']

        # Get indices where we have time average variables
        indices = config.instant

        # Append short_name with _tavg for these variables
        for ni in np.nditer(indices):
            # Find variable that correspond to array[ni] (might have different cap / un-cap mix)!
            netcdf_name = config.match_config_netcdf_names(array[ni])
            self.renameVariable(netcdf_name, array[ni] + '_inst')

    def expand_var_names(self,config):
        ''' Add _tavg or _inst at the end of short name depending on configuration file'''
        # time average variables.
        self.expand_time_avg(config)

        # instantaneous variables. They are the ones that are not time average!
        self.expand_instant(config)
    
    def rename_coordinates(self):
        ''' Use the following table for renaming: 
        LATITUDE -> lat
        LONGITUDE -> lon'''
        self.renameVariable("LATITUDE","lat")
        self.renameVariable("LONGITUDE","lon")

    def add_var_atts(self, config):
        ''' Need to set: 
          - standard_name -> from database 
          - long_name -> from database
          - scale_factor = 1.f
          - add_offset = 0.f
          - missing_value -> from file attribute
          - _FillValue -> same as missing_value
          - vmin = -1.e+15f
          - vmax = 1.e+15f '''
        
        # Create attribute dictionary for attributes common to all var.
        attrs = {'scale_factor' : 1.,                 \
                 'add_offset'   : 0.,                 \
                 'missing_value': self.missing_value, \
                 '_FillValue'   : self.missing_value, \
                 'vmin'         : -1.e+15,            \
                 'vmax'         : 1.e15,              \
                }

        # Loop over variables
        for var in self.variables.keys():
            self.variables[var].setncatts(attrs) # Set all common attributes at once
   
    def add_long_name(self):
        ln_dict = self.long_name_dict()
        # Deal with the upper-case / lower-case issue.
        if 'SWnet' in self.variables.keys():
            ln_dict['SWnet'] = ln_dict.pop('Swnet')
        if 'LWnet' in self.variables.keys():
            ln_dict['LWnet'] = ln_dict.pop('Lwnet')
        
        for key,val in ln_dict.iteritems():
            if self.variables.has_key(key):
                self.variables[key].long_name = val

    def add_standard_name(self):
        sn_dict = self.standard_name_dict()
        # Deal with the upper-case / lower-case issue.
        if 'SWnet' in self.variables.keys():
            sn_dict['SWnet'] = sn_dict.pop('Swnet')
        if 'LWnet' in self.variables.keys():
            sn_dict['LWnet'] = sn_dict.pop('Lwnet')
        
        for key,val in sn_dict.iteritems():
            if self.variables.has_key(key):
                self.variables[key].standard_name = val
        
    def rename_dimensions(self, dims={ "north-south":"north_south" \
                                     , "east-west"  :"east_west"   \
                                     , "soilm_profiles": "SoilMoist_profiles" \
                                     , "soilt_profiles": "SoilTemp_profiles"  \
                                     }):
        """Rename dimensions in self. 
        dims: optional dictionary. keys are the old dimension name, value is the new name"""

        for old_dim, new_dim in dims.iteritems():
            if old_dim in self.dimensions.keys():
                self.renameDimension(old_dim,new_dim)



class LIS_Model_Output(np.ndarray):
    def __new__(cls, filename):
        dtype = np.dtype({'names': ('short_name', 'select',     \
                           'units', 'signconv', 'timeavg',      \
                           'min/max', 'std', 'vert.levels',     \
                           'grib_id', 'grib_scalefactor'),      \
                          'formats':('S13','i1','S8','S3','i1', \
                                     'i1','i1','i2','i3','i7')  \
                         })
        obj = np.loadtxt(filename, dtype, comments='#').view(cls)
        obj.info = 'from file {}'.format(filename)

        # Get colon out of 'short_name'
        obj.remove_last_character('short_name')

        # Get indices for selected variables, time averaged variables
        # and instantaneous variables.
        obj.select    = obj.indices_select()
        obj.time_avg  = obj.indices_period('timeavg')
        obj.instant   = obj.indices_period('instant')
        
        return obj

    def __array_finalize__(self, obj):
        # From an explicit constructor. e.g LIS_Model_Output()
        # obj is None
        # so we need to return to __new__ method to finish initialisation
        if obj is None: return
        # But in other cases obj is not none but we have nothing to add except
        # the stub info attribute to make things clear.
        # None is the default value used if obj is not a 
        # LIS_Model_Output
        self.info     = getattr(obj, 'info', None)
        self.select   = getattr(obj, 'select', ())
        self.time_avg = getattr(obj, 'time_avg', ())
        self.instant  = getattr(obj, 'instant', ())

    def __array_wrap__(self, out_arr, context=None):
        return np.ndarray.__array_wrap__(self, out_arr, context)


    def remove_last_character(self,field=str,character=':'):
        ''' Remove last character of a string field. Does nothing if last 
        character is not equal to the character keyword. '''
        # Use item and itemset methods to avoid differentiating in case of 0-d array.
        # We have a 0-d array if file has only 1 line.
        # As per documentation, it is better to save the methods in local var.
        # before the loop then use the local vars. in the loop.
        item_loc    = self[field].item
        itemset_loc = self[field].itemset

        # Check type of field:
        if not(isinstance(item_loc(0), str)):
            exit()
            # Error not a string!

        for nrow in xrange(self.size):
            # This check is just to make sure we only remove the
            # last character if it is the character asked for.
            if item_loc(nrow)[-1] == character:
                itemset_loc(nrow, item_loc(nrow)[:-1]) 

    def indices_select(self):
        ''' Get indices of selected variables 
        select: tuple of arrays with indices of selected variables'''

        select = np.where(self['select'])
        return select[0]

    def indices_period(self, period=''):
        '''Get variable indices that are time averaged
        period: either 'timeavg' or 'instant'
        indices: tuple of arrays with indices of variables for this period
                 and selected!'''

        # Check input argument value
        if not(period in ['timeavg', 'instant']):
            exit()
            # Error! How to do that?

        # Check if we need to call indices_select()
        if not(hasattr(self, 'select')):
            # Get selected variables first
            self.select = self.indices_select()

        # Get timeavg field of selected variables
        # Case where no variables selected:
        if self.select.size == 0:
            return self.select

        else:
            if self.size == 1:
                # We have only 1 element. Because we are in the 'else' statement
                # we know this variable is selected so just use the special indexing
                # to get the 'timeavg' element
                a = self[...]['timeavg']
            else:
                a = self[self.select]['timeavg']

            if period == 'timeavg':
                indices = self.select[np.where(a == 1)]
            else:
                indices = self.select[np.where(a == 0)]

            return indices


    @staticmethod
    def match_config_netcdf_names(name=str):
        ''' Get a variable name from the LIS output config file to the name used in
        the old version of the LIS netcdf file. Some capitalisations are different '''
        if name in ['Swnet','Lwnet']:
            
            if name == 'Swnet':
                new_name = 'SWnet'
            else:
                new_name = 'LWnet'
                
            return new_name

        else:
            # No change needed
            return name
        
