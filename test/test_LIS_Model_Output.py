import numpy as np
from numpy.testing import assert_array_equal
from StringIO import StringIO
import sys
sys.path.append('/short/w35/ccc561/LIS/Python_lib/lib')
import reformat_LIS_lib as l


dtype = np.dtype({'names': ('short_name', 'select',      \
                   'units', 'signconv', 'timeavg',       \
                   'min/max', 'std', 'vert.levels',      \
                   'grib_id', 'grib_scalefactor'),       \
                  'formats':('S13','i1','S8','S3','i1',  \
                             'i1','i1','i2','i3','i7')   \
                 })

# Multiple line file
multi_array = np.array([('A',1,"'W/m2'",'DN',1,0,0,6,10,100000),('B',0,"'kg/m2s'",'LPV',0,0,0,1,10,100000),('C',1,"'kg/m2s'",'LPV',0,0,0,1,10,100000)],dtype=dtype)

c_multi = "A: 1 'W/m2' DN 1 0 0 6 10 100000 \nB: 0 'kg/m2s' LPV 0 0 0 1 10 100000\nC: 1 'kg/m2s' LPV 0 0 0 1 10 100000"

# Single line file
c_single = "A: 1 'W/m2' DN 1 0 0 6 10 100000 "
single_array = np.array([('A',1,"'W/m2'",'DN',1,0,0,6,10,100000)],dtype=dtype)

# 0 selected
c_zero = "A: 0 'W/m2' DN 1 0 0 6 10 100000 \nB: 0 'kg/m2s' LPV 0 0 0 1 10 100000\nC: 0 'kg/m2s' LPV 0 0 0 1 10 100000"
 
def test_multi_lines_array():
    f = open('temp', 'w')
    f.write(c_multi)
    f.close()
    test_array = l.LIS_Model_Output('temp')
    assert_array_equal(test_array, multi_array)

def test_multi_lines_info():
    f = open('temp', 'w')
    f.write(c_multi)
    f.close()
    test_array = l.LIS_Model_Output('temp')
    assert( test_array.info == 'from file temp' )

def test_multi_lines_select():
    f = open('temp', 'w')
    f.write(c_multi)
    f.close()
    test_array = l.LIS_Model_Output('temp')

    assert_array_equal( test_array.select, np.asarray([0,2]))

def test_multi_lines_avg():
    f = open('temp', 'w')
    f.write(c_multi)
    f.close()
    test_array = l.LIS_Model_Output('temp')

    assert( test_array.time_avg == np.asarray([0]))

def test_multi_lines_instant():
    f = open('temp', 'w')
    f.write(c_multi)
    f.close()
    test_array = l.LIS_Model_Output('temp')

    assert( test_array.instant == np.asarray([2]))

def test_single_line_array():
    # One line file
    f = open('temp', 'w')
    f.write(c_single)
    f.close()
    
    test_array = l.LIS_Model_Output('temp')
    assert_array_equal(test_array, single_array)

def test_single_line_select():
    # One line file
    f = open('temp', 'w')
    f.write(c_single)
    f.close()
    
    test_array = l.LIS_Model_Output('temp')

    assert( test_array.select == np.asarray([0]))

def test_single_line_avg():
    # One line file
    f = open('temp', 'w')
    f.write(c_single)
    f.close()
    
    test_array = l.LIS_Model_Output('temp')

    assert( test_array.time_avg == np.asarray([0]))

def test_single_line_instant():
    # One line file
    f = open('temp', 'w')
    f.write(c_single)
    f.close()
    
    test_array = l.LIS_Model_Output('temp')

    assert( test_array.instant.size == 0)
    

def test_time_avg_0_select():
    f = open('temp', 'w')
    f.write(c_zero)
    f.close()
    
    test_array = l.LIS_Model_Output('temp')
    assert( test_array.time_avg.size == 0)
    

