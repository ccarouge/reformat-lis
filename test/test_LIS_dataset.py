import numpy as np
import netCDF4
import sys
sys.path.append('/short/w35/ccc561/LIS/Python_lib/lib')
import reformat_LIS_lib as l
import shutil

def test_expand_time_avg():
    shutil.copy('CABLE.total.d01.nc','tmp.nc')
    obj = l.LIS_Dataset('tmp.nc','r+')
    config = l.LIS_Model_Output('MODEL_OUTPUT_LIST.TBL')

    obj.expand_time_avg(config)
    assert( obj.variables.keys() == [u'LATITUDE', u'LONGITUDE', u'Albedo', u'SWE', u'SoilMoist', u'SoilTemp', u'CanopInt', u'Wind_f', u'Rainf_f', u'Tair_f', u'Qair_f', u'Psurf_f', u'SWdown_f', u'LWdown_f', u'Landmask', u'Landcover', u'SandFrac', u'ClayFrac', u'SiltFrac', u'Elevation', u'LAI', 'Swnet_tavg', 'Lwnet_tavg', 'Qle_tavg', 'Qh_tavg', 'Qg_tavg', 'Snowf_tavg', 'Rainf_tavg', 'Evap_tavg', 'Qs_tavg', 'Qsb_tavg', 'SnowT_tavg', 'VegT_tavg', 'BareSoilT_tavg', 'ECanop_tavg', 'TVeg_tavg', 'ESoil_tavg'] )

def test_rename_coordinates():
    shutil.copy('CABLE.total.d01.nc','tmp.nc')
    obj = l.LIS_Dataset('tmp.nc','r+')

    obj.rename_coordinates()
    assert('lat' in obj.variables.keys())
    assert('lon' in obj.variables.keys())

def test_add_var_atts():
    shutil.copy('CABLE.total.d01.nc','tmp.nc')
    obj = l.LIS_Dataset('tmp.nc','r+')
    config = l.LIS_Model_Output('MODEL_OUTPUT_LIST.TBL')

    obj.add_var_atts(config)
    print obj.variables['LATITUDE'].__dict__
    assert(obj is None)
